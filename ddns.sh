#!/bin/bash

domain='';
pass_key='';

current_ip='';
new_ip=$(curl -s https://api.ipify.org/);

ddns_url="https://www.ddnss.de/upd.php?key=$pass_key&host=$domain&ip=$new_ip";

path="./";

files="bin/";
old_ip_file="old_ip.txt";
log_file="ddns_log.txt";

counter=1;
max_tries=10;

update_old_ip() {
	echo "$new_ip" > "$path$files$old_ip_file";
	echo "$(date) $current_ip > $new_ip" >> "$path$files$log_file";
}


if [ ! -f "$path$files$old_ip_file" ]
then
	mkdir $path$files;
	touch $path$files$old_ip_file;
fi

if [ ! -f "$path$files$log_file" ]
then
	touch $path$files$log_file;
fi

while [ "$counter" -lt "$max_tries" ]
do
	while read ip; do
		current_ip="$ip"
	done < $path$files$old_ip_file;

        if [ "$current_ip" != "$new_ip" ]
	then
		echo trying update ip address for $domain
		result=$(curl -s $ddns_url);
		counter=$((counter+1));

		if [[ $result == *"Updated"* ]]
		then
			echo updated ip address for $domain;
			counter=$max_tries;
			update_old_ip;
		else
			echo could not update ip address for $domain;
			echo retry;
		fi

	else
		echo no ip update needed for $domain;
		counter=$max_tries;
	fi
done
