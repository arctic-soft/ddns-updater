## ddns-updater

Updater for automatically refreshing of public ip address for dynamic dns (https://www.ddnss.de/)

* clone repo and add domain and pass_key in ddns.sh  

* Run following command to check and update the public ip address

```
sudo chmod + x ddnss.sh && sudo ./ddns.sh
```

* Tested with https://www.ddnss.de
